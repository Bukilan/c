/*
������:
-������� ����� ������� � ������������� � ������������
-����������� ��������� (+/-/*/+=/-=/*=)
-����������� �������� () ��� ���������� ��������� �������
-������� ����� ���������������� ������� 
-������� ����� dot  ��� ������������ ������ 
-����������� ��������� << � >>  

������� ����
������ ��� ��������: ���������� ����� � �������� 
����������� �������� ������������ �� ���� ���� ������� 

�������� ����
-������������ �������
-������� ����������� �� 2 
-������� ����� �������� � 3
-������� (1;3) ������������ ������� 
-���������������� �������
-������������ �������, ������� � main � ��������� ������� 

������ ������
3 3
1 2 3
4 5 6
7 8 9
}{
ORIGINAL MATRIX:
1 2 3
4 5 6
7 8 9

MULTIPLY BY 2:
2 4 6
8 10 12
14 16 18

SUBTRACT 3:
-1 1 3
5 7 9
11 13 15

ELEMENT AT (1,3):
3

TRANSPOSE:
-1 5 11
1 7 13
3 9 15

MATRIX MULTIPLICATION:
70 -37
110 1
150 39
*/

#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>

#define INPUT_FILE "input.txt"

using namespace std;

const int BUFF_SIZE = 256;

class Matrix {

private:
    int n, m;
    vector<int> container;

public:

    Matrix() {
        n = 0;
        m = 0;
    }

    Matrix(int n, int m) {	
        this->n = n;
        this->m = m;
        container.resize(n * m);
    }

    Matrix(int n, int m, vector<int> container) {
        this->n = n;
        this->m = m;
        this->container = container;
    }

    Matrix(const Matrix &matrix) {
        this->n = matrix.n;
        this->m = matrix.m;
        this->container = matrix.container;
    }

    Matrix operator +(const int scal) const { return Matrix(*this) += scal; }
    Matrix operator -(const int scal) const { return Matrix(*this) -= scal; }
    Matrix operator *(const int scal) const { return Matrix(*this) *= scal; }

    Matrix &operator +=(const int scal) {
        for (int i = 0; i < n * m; i++) {
            container[i] += scal;
        }
        return *this;
    }

    Matrix &operator -=(const int scal) {
        for (int i = 0; i < n * m; i++) {
            container[i] -= scal;
        }
        return *this;
    }

    Matrix &operator *=(const int scal) {
        for (int i = 0; i < n * m; i++) {
            container[i] *= scal;
        }
        return *this;

    }

    int operator()(const int i, const int j) const {
        if (i > n || j > m || i < 0 || j < 0) {
            return -1;
        }
        else {
            return container[(i - 1) * m + j - 1];
        }
    }

    int &operator()(const int i, const int j) {
        if (i > n || j > m || i < 0 || j < 0) {
            int value = -1;
            int &ref = value;
            return ref;
        }
        else {
            return container[(i - 1) * m + j - 1];
        }
    }

    Matrix getTransposed() {
        Matrix newMatrix = Matrix(m, n);
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                newMatrix(i, j) = (*this)(j, i);
            }
        }
        return newMatrix;
    }

    Matrix operator *(const Matrix &matrix) {
        if (m == matrix.getN()) {
            Matrix newMatrix = Matrix(n, matrix.getM());
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= matrix.getM(); j++) {
                    newMatrix(i, j) = 0;
                    for (int k = 1; k <= m; k++) {
                        newMatrix(i, j) += (*this)(i, k) * matrix(k, j);
                    }
                }
            }
            return newMatrix;
        }
        else {
            cout << "Unable to multiply." << endl;
            return *this;
        }
    }

    friend ostream& operator<<(ostream& os, Matrix &matrix);
    friend istream& operator>>(istream& is, Matrix &matrix);

    int getN() const { return n; }
    int getM() const { return m; }

    ~Matrix() {
        container.clear();
        container.shrink_to_fit();
    }
};

ostream& operator<<(ostream& os, Matrix &matrix) {
    for (int i = 1; i <= matrix.getN(); i++) {
        for (int j = 1; j <= matrix.getM(); j++) {
            os << matrix(i, j) << ' ';
        }
        os << '\n';
    }
    return os;
}

istream& operator>>(istream& is, Matrix &matrix) {
	int n = 0;
	int m = 0;
	vector<int> newContainer;
	
	is >> n >> m;
	
	matrix.n = n;
	matrix.m = m; 
	
	
	
	for (int i = 1 ; i <= (n * m) ; i++){
		
		int currentnumber;
		is >> currentnumber;
		matrix.container.push_back(currentnumber);
	}
	
	return is;
}
    /*vector<int> newContainer;
    char buffer[BUFF_SIZE];
    int n = 0;
    int m = 0;

    while (is.getline(buffer, BUFF_SIZE)) {
        stringstream stream(buffer);
        int currentNumber;
        while (stream >> currentNumber) {
            newContainer.push_back(currentNumber);
            if (n == 0) m++;
        }
        n++;
    }
    matrix = Matrix(n, m, newContainer);
    return is;
}
*/


int main() {

    auto inputMatrix = Matrix();

    ifstream input;
    input.open(INPUT_FILE);
    input >> inputMatrix;
    input.close();

    cout << "ORIGINAL MATRIX:\n";
    cout << inputMatrix;

    cout << "\nMULTIPLY BY 2:\n";
    Matrix matrix = inputMatrix * 2;
    cout << matrix;

    cout << "\nSUBTRACT 3:\n";
    matrix -= 3;
    cout << matrix;
    
    cout << "\nELEMENT AT (1,3):\n";
    Matrix outMatrix = matrix;
    cout << outMatrix(1, 3) << "\n";

    cout << "\nTRANSPOSE:\n";
    matrix = matrix.getTransposed();
    cout << matrix;

    cout << "\nMATRIX MULTIPLICATION:\n";
    vector<int> data(6);
    data[0] = 5;
    data[1] = 17;
    data[2] = 15;
    data[3] = 7;
    data[4] = 0;
    data[5] = -5;
    Matrix multiplier = Matrix(3, 2, data);
    Matrix dotResult = matrix * multiplier;
    cout << dotResult;

    return 0;
}
