/*
������:
������� �������� ��� �������������������� ��������� ������ ������

������� �����
-� ������ ������� ���������� ������ ������
-�������� ���� : ���� , ��������

�������� �����
�������� ������ ���������� � ������� , ��������������� �������

������
Input
3
1 5
2 51
3 212
Output
5 51 212
*/

#include <iostream>
#include <fstream>
#include "iterator.h"
#include "rbtree.h"

using namespace std;

int main()
{
    ifstream fin ("input.txt");
    ofstream fout ("output.txt");

    RBTree <int , int> test;
    cout << test.getSize() << endl;
    Iterator<int , int> i;
    int n , key , content;
    fin >> n;
    for(int i = 0 ; i < n ; i++){
        fin >> key >> content;
        test.rb_insert(key , content);
    }
    for(i = test._begin() ; i != test._end() ; i++){
        cout << *i << " ";
    }
    fin.close();
    fout.close();
    return 0;
}
