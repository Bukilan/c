#ifndef RBTREE_H_INCLUDED
#define RBTREE_H_INCLUDED

#include <iostream>
#include "iterator.h"

using namespace std;

template<typename T , typename K>
class Iterator;

enum Color{
    red   ,
    black ,
};

template<typename T , typename K>
struct RBNode{
    K key;
    T content;
    RBNode<T , K> *left   ,
                  *right  ,
                  *parent ;
    Color color;
    T& getContent();
    RBNode();
    RBNode(const K& key , const T& content);
};

RBNode<int , int>* NIL = new RBNode<int , int>();

template<typename T , typename K>
class RBTree{
    friend class Iterator<T , K>;
private:
    RBNode<T , K>* root;
    int tree_size;
    void left_rotate(RBNode<T , K>* x);
    void right_rotate(RBNode<T , K>* x);
    void rb_transplant(RBNode<T , K>* u , RBNode<T , K>* v);
    void rb_insert_fixup(RBNode<T , K>* z);
    void rb_delete_fixup(RBNode<T , K>* x);
    void IteratorAssign(Iterator<T , K> from , Iterator<T , K> to);
    RBNode<T , K>* minimum(RBNode<T , K>* x);
    RBNode<T , K>* successor(RBNode<T , K>* z);
    RBNode<T , K>* predessor(RBNode<T , K>* z);
    void tree_destroy(RBNode<T , K>* leaf);
public:
    RBTree();
    RBTree(RBTree<T , K>& other);
    ~RBTree();
    void rb_insert(const K& key , const T& content);
    void rb_delete(const K& key);
    RBNode<T , K>* element_search(K key);
    int getSize();
    Iterator<T , K> _begin();
    Iterator<T , K> _end();
};

template<typename T , typename K>
T& RBNode<T , K>::getContent(){
    return this -> content;
}


template<typename T , typename K>
RBNode<T , K>::RBNode()
: left(NIL)
, right(NIL)
, parent(NIL)
, color(black)
{}

template<typename T , typename K>
RBNode<T , K>::RBNode(const K& key , const T& content)
: key(key)
, content(content)
, left(NIL)
, right(NIL)
, parent(NIL)
, color(black)
{}

template<typename T , typename K>
RBTree<T , K>::RBTree()
: root(NIL)
, tree_size(0)
{}

template<typename T , typename K>
RBTree<T , K>::RBTree(RBTree<T , K>& other)
: root(other.root)
, tree_size(other.tree_size)
{}

template<typename T , typename K>
RBTree<T , K>::~RBTree(){
    tree_destroy(root);
}

template<typename T , typename K>
void RBTree<T , K>::left_rotate(RBNode<T , K>* x){
    RBNode<T , K>* y = x -> right;
    x -> right = y -> left;
    if(y -> left != NIL){
        y -> left -> parent = x;
    }
    y -> parent = x -> parent;
    if(x -> parent == NIL){
        this -> root = y;
    }
    else if(x == x -> parent -> left){
        x -> parent -> left = y;
    }
    else{
        x -> parent -> right = y;
    }
    y -> left = x;
    x -> parent = y;
}

template<typename T , typename K>
void RBTree<T , K>::right_rotate(RBNode<T , K>* x){
    RBNode<T , K>* y = x -> left;
    x -> left = y -> right;
    if(y -> right != NIL){
        y -> right -> parent = x;
    }
    y -> parent = x -> parent;
    if(x -> parent == NIL){
        this -> root = y;
    }
    else if(x == x -> parent -> left){
        x -> parent -> left = y;
    }
    else{
        x -> parent -> right = y;
    }
    y -> right = x;
    x -> parent = y;
}

template<typename T , typename K>
void RBTree<T , K>::rb_transplant(RBNode<T , K>* u , RBNode<T , K>* v){
    if(u -> parent == NIL){
        this -> root = v;
    }
    else if(u == u -> parent -> left){
        u -> parent -> left = v;
    }
    else{
        u -> parent -> right = v;
    }
    v -> parent = u -> parent;
}

template<typename T , typename K>
void RBTree<T , K>::rb_insert_fixup(RBNode<T , K>* z){
    RBNode<T , K>* y;
    while(z -> parent -> color == red){
        if(z -> parent == z -> parent -> parent -> left){
            y = z -> parent -> parent -> right;
            if(y -> color == red){
                z -> parent -> color = black;
                y -> color = black;
                z -> parent -> parent -> color = red;
                z = z -> parent -> parent;
            }
            else{
                if(z == z -> parent -> right){
                    z = z -> parent;
                    left_rotate(z);
                }
                z -> parent -> color = black;
                z -> parent -> parent -> color = red;
                right_rotate(z -> parent -> parent);
            }
        }
        else{
            y = z -> parent -> parent -> left;
            if(y -> color == red){
                z -> parent -> color = black;
                y -> color = black;
                z -> parent -> parent -> color = red;
                z = z -> parent -> parent;
            }
            else{
                if(z == z -> parent -> left){
                    z = z -> parent;
                    right_rotate(z);
                }
                z -> parent -> color = black;
                z -> parent -> parent -> color = red;
                z -> parent -> left -> color = black;
                left_rotate(z -> parent -> parent);
            }
        }
    }
    this -> root -> color = black;
}

template<typename T , typename K>
void RBTree<T , K>::rb_delete_fixup(RBNode<T , K>* x){
    RBNode<T , K> *w;
    while(x != this -> root && x -> color == black){
        if(x == x -> parent -> left){
            w = x -> parent -> right;
            if(w -> color == red){
                w -> color = black;
                x -> parent -> color = red;
                left_rotate(x -> parent);
                w = x -> parent -> right;
            }
            if(w -> left -> color == black && w -> right -> color == black){
                w -> color == red;
                x = x -> parent;
            }
            else{
                if(w -> right -> color == black)
                {
                    w -> left -> color = black;
                    w -> color = red;
                    right_rotate(w);
                    w = x -> parent -> right;
                }
                w -> color = x -> parent -> color;
                x -> parent -> color = black;
                x -> right -> color = black;
                left_rotate(x -> parent);
                x = this -> root;
            }
        }
        else{
            w = x -> parent -> left;
            if(w -> color == red){
                w -> color = black;
                x -> parent -> color = red;
                right_rotate(x -> parent);
                w = x -> parent -> left;
            }
            if(w -> right -> color == black && w -> left -> color == black){
                w -> color = red;
                x = x -> parent;
            }
            else{
                if(w -> left -> color == black){
                    w -> right -> color = black;
                    w -> color = red;
                    left_rotate(w);
                    w = x -> parent -> left;
                }
                w -> color = x -> parent -> color;
                x -> parent -> color = black;
                w -> left -> color = black;
                right_rotate(x -> parent);
                x = this -> root;
            }
        }
    }
}

template<typename T , typename K>
RBNode<T , K>* RBTree<T , K>::minimum(RBNode<T , K>* x){
    return x -> left != NIL ? minimum(x -> left) : x;
}

template<typename T , typename K>
RBNode<T , K>* RBTree<T , K>::successor(RBNode<T , K>* z){
    RBNode<T , K>* current = z     ,
                   successor = NIL ;
    while(current != NIL)
    {
        if(current -> key > z -> key){
            successor = current;
            current = current -> left;
        }
        else{
            current = current -> right;
        }
    }
    return successor;
}

template<typename T , typename K>
RBNode<T , K>* RBTree<T , K>::predessor(RBNode<T , K>* z){
    RBNode<T , K>* current = z     ,
                   predessor = NIL ;
    while(current != NIL){
        if(current -> key < z -> key){
            successor = current;
            current = current -> right;
        }
        else{
            current = current -> left;
        }
    }
    return predessor;
}

template<typename T , typename K>
void RBTree<T , K>::tree_destroy(RBNode<T , K>* leaf){
    if(leaf != NIL){
        tree_destroy(leaf -> left);
        tree_destroy(leaf -> right);
        delete leaf;
    }
}

template<typename T , typename K>
void RBTree<T , K>::rb_insert(const K& key , const T& content){
    RBNode<T , K> *z = new RBNode<T , K>(key , content) ,
                  *y = NIL                              ,
                  *x = this -> root;                    ;
    while(x != NIL){
        y = x;
        if(z -> key == x -> key){
            return;
        }
        if(z -> key < x -> key){
            x = x -> left;
            continue;
        }
        if(z -> key > x -> key){
            x = x -> right;
            continue;
        }
    }
    z -> parent = y;
    if(y == NIL){
        this -> root = z;
    }
    else{
        if(z -> key < y -> key){
            y -> left = z;
        }
        else{
            y -> right = z;
        }
    }
    z -> color = red;
    rb_insert_fixup(z);
    this -> tree_size++;
}

template<typename T , typename K>
void RBTree<T , K>::rb_delete(const K& key){
    RBNode<T , K> *x                               ,
                  *y                               ,
                  *z = this -> element_search(key) ;
    if(z == NULL){
        return;
    }
    if(z -> left == NIL || z -> right == NIL){
        y = z;
    }
    else{
        y = this -> successor(z);
    }
    if(y -> left != NIL){
        x = y -> left;
    }
    else{
        x = y -> right;
    }
    x -> parent = y -> parent;
    if(y -> parent == NIL){
        this -> root = x;
    }
    else{
        if(y == y -> parent -> left){
            y -> Parent -> left = x;
        }
        else{
            y -> parent -> right = x;
        }
    }
    if(y != z){
        z -> key = y -> key;
    }
    if(y -> color == black){
        rb_delete_fixup(x);
    }
    this -> tree_size--;
}

template<typename T , typename K>
void RBTree<T , K>::IteratorAssign(Iterator<T , K> from , Iterator<T , K> to){
    for(Iterator<T , K> i = from ; i != to ; i++){
        cout << *i << " ";
    }
}

template<typename T , typename K>
RBNode<T , K>* RBTree<T , K>::element_search(K key){
    RBNode<T , K>* current = this -> root;
    while(current != NIL && current -> key != key){
        current = current -> key > key ? current -> left : current -> right;
    }
    return current == NIL ? NULL : current;
}

template<typename T , typename K>
int RBTree<T , K>::getSize(){
    return this -> tree_size;
}

template<typename T , typename K>
Iterator<T , K> RBTree<T , K>::_begin(){
    Iterator<T , K> result;
    return result._begin(this , this -> root);
}

template<typename T , typename K>
Iterator<T , K> RBTree<T , K>::_end(){
    Iterator<T , K> result;
    return result._end(this , this -> root);
}

#endif 
