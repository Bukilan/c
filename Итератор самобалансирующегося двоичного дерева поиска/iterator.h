#ifndef ITERATOR_H_INCLUDED
#define ITERATOR_H_INCLUDED

#include <iostream>
#include "rbtree.h"

using namespace std;

template<typename T , typename K>
struct RBNode;

template<typename T , typename K>
class RBTree;

template<typename T , typename K>
class Iterator{
    friend class RBTree<T , K>;
private:
    const RBTree<T , K>* instance;
    RBNode<T , K>* currentNode;
public:
    Iterator();
    Iterator(const RBTree<T , K>* instance , RBNode<T , K>* initialNode);
    ~Iterator();
    Iterator<T , K>& operator = (const Iterator<T , K>& other);
    Iterator<T , K>& operator ++ ();
    Iterator<T , K> operator ++ (int);
    Iterator<T , K> operator -- ();
    Iterator<T , K> operator -- (int);
    T& operator * ();
    RBNode<T , K>* node();
    bool operator == (const Iterator<T , K>& other) const;
    bool operator != (const Iterator<T , K>& other) const;
    Iterator<T , K> _begin(const RBTree<T , K>* instance , RBNode<T , K>* root);
    Iterator<T , K> _end(const RBTree<T , K>* instance , RBNode<T , K>* root);
};

template<typename T , typename K>
Iterator<T , K>::Iterator()
: instance(NULL)
, currentNode(NULL)
{}

template<typename T , typename K>
Iterator<T , K>::Iterator(const RBTree<T , K>* instance , RBNode<T , K>* initialNode)
: instance(instance)
, currentNode(initialNode)
{}

template<typename T , typename K>
Iterator<T , K>::~Iterator()
{}

template<typename T , typename K>
Iterator<T , K>& Iterator<T , K>::operator = (const Iterator<T , K>& other){
    this -> instance = other.instance;
    this -> currentNode = other.currentNode;
}

template<typename T , typename K>
Iterator<T , K>& Iterator<T , K>::operator ++ (){
    RBNode<T , K> *current = this -> currentNode,
                  *successor = NIL              ;
    while(current != NIL && current -> parent != NIL){
        current = current -> parent;
    }
    while(current != NIL){
        if(current -> key > this -> currentNode -> key){
            successor = current;
            current = current -> left;
        }
        else{
            current = current -> right;
        }
    }
    this -> currentNode = successor;
    return *this;
}

template<typename T , typename K>
Iterator<T , K> Iterator<T , K>::operator ++ (int){
    Iterator<T , K> old = *this;
    ++(*this);
    return old;
}

template<typename T , typename K>
Iterator<T , K> Iterator<T , K>::operator -- (){
    RBNode<T , K> *current = this -> currentNode      ,
                  *predessor = NIL                    ;
    while(current != NIL && current -> parent != NIL){
        current = current -> parent;
    }
    while(current != NIL){
        if(current -> key < this -> currentNode -> key){
            predessor = current;
            current = current -> right;
        }
        else{
            current = current -> left;
        }
    }
    this -> currentNode = predessor;
    return *this;
}

template<typename T , typename K>
Iterator<T , K> Iterator<T , K>::operator -- (int){
    Iterator<T , K> old = *this;
    --(*this);
    return old;
}

template<typename T , typename K>
T& Iterator<T , K>::operator * (){
    return this -> currentNode -> getContent();
}


template<typename T , typename K>
RBNode<T , K>* Iterator<T , K>::node(){
    return this -> currentNode;
}

template<typename T , typename K>
bool Iterator<T , K>::operator == (const Iterator<T , K>& other) const{
    return (this -> instance == other.instance && this -> currentNode == other.currentNode);
}

template<typename T , typename K>
bool Iterator<T , K>::operator != (const Iterator<T , K>& other) const{
    return !(*this == other);
}

template<typename T , typename K>
Iterator<T , K> Iterator<T , K>::_begin(const RBTree<T , K>* instance , RBNode<T , K>* root){
    RBNode<T , K>* current = root;
    while(current != NIL && current -> left != NIL){
        current = current -> left;
    }
    return Iterator<T , K>(this -> instance , current);
}

template<typename T , typename K>
Iterator<T , K> Iterator<T , K>::_end(const RBTree<T , K>* instance , RBNode<T , K>* root){
    return Iterator<T , K>(this -> instance , NIL);
}

#endif 
