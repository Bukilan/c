/*
������:
���������� xml-���� � �� ���������� ��������� �� ����� , ������� ������ ���� ������	

������� ����
xml-���� + ��������� ���������� ������ �  �������� ����������

�������� ����
���� ������� ������

������

Input
app.exe -data=daily,xml -from=AUD -to=RUB
Output
You can have 47,5834 RUB for one AUD
*/

#include <iostream>
#include <libxml++/libxml++.h>
#include <string>

class Valute{
private:
    std::string filename;
    xmlpp::DomParser domparser;
    xmlpp::Node* parentNode;
public:
    Valute(const std::string &filename)
            : filename(filename){
        try{
            domparser.set_validate(false);
            domparser.set_substitute_entities();
            domparser.parse_file(filename);
            if(!domparser){
                throw std::runtime_error("file is damaged");
            }
        }
        catch(const std::exception ex){
            std::cout << "Exception caught: " << ex.what() << std::endl;
        }
        parentNode = domparser.get_document() -> get_root_node();
    }
    ~Valute() = default;

    float ustring_to_int(const Glib::ustring &course);
    float get_valute(const Glib::ustring &val);
    float convert(const Glib::ustring &from , const Glib::ustring &to);
};

float Valute::ustring_to_int(const Glib::ustring &course){
    int result = 0 , k = 10;
    bool aftercomma = false;
    for(auto c : course){
        if(c == ','){
            aftercomma = true;

            continue;
        }
        if(!aftercomma){
            result = result * 10 + c - '0';
        }
        else{
            result += (c - '0') / k;
            k *= 10;
        }
    }
    return result;
}

float Valute::get_valute(const Glib::ustring &valute_name){
    bool found = false;
    auto valute_list = this -> parentNode -> get_children();
    for(auto valute : valute_list){
        auto children = valute -> get_children();
        for(auto name : children){
            if(found && name -> get_name() == "Value"){
                auto elementNode = dynamic_cast<xmlpp::Element*>(name);
                return ustring_to_int(elementNode -> get_first_child_text() -> get_content());
            }
            if(name -> get_name() == "CharCode"){
                auto charcode = dynamic_cast<xmlpp::Element*>(name);
                if(charcode -> get_first_child_text() -> get_content() == valute_name){
                    found = true;
                }
            }
        }
        if(found){
            std::cout << "No data" << std::endl;
            return 0;
        }
    }
    std::cout << "No such valute" << std::endl;
    return 0;
}

float Valute::convert(const Glib::ustring &from, const Glib::ustring &to){
    float value_from = this -> get_valute(from);
    float value_to = this -> get_valute(to);
    if(value_from && value_to){
        return (value_from / value_to);
	}
    else{
        return 0;
    }
}

std::string get_filename(const std::string data){
    return data.substr(data.find("=") + 1);
}

Glib::ustring get_data(const std::string data){
    return data.substr(data.find("=") + 1);
}

int main(int argc , char* argv[]){
    if(argc < 4){
        std::cout << "Wrong input!" << std::endl;
        std::cout << "Correct input is: ./app -data=filename.smth -from=SMTH -to=SMTH" << std::endl;
    }
    else{
        std::string filename = get_filename(argv[1]);
        Glib::ustring from = get_data(argv[2]);
        Glib::ustring to = get_data(argv[3]);
        Valute converter(filename);
        float result = converter.convert(from , to);
        if(result){
            std::cout << "You can have " << result << " " << to << " for one " << from << std::endl;
        }
    }
    return 0;
}
